core = 7.x
api = 2

; MUR CRM signup module
projects[uw_mur_crm_signup][type] = "module"
projects[uw_mur_crm_signup][download][type] = "git"
projects[uw_mur_crm_signup][download][url] = "https://git.uwaterloo.ca/mur-dev/uw_mur_crm_signup.git"
projects[uw_mur_crm_signup][download][tag] = "7.x-1.0.3"
projects[uw_mur_crm_signup][subdir] = ""